#include "ComponentModel\ComponentManager.h"
#include "Source\ComponentModel\Components\Types.h"

int main()
{
	using namespace ComponentModel;
	using namespace Component;
	ComponentManager cm;
	
	Entity& player = cm.createEntity("Player");
	Entity& enemy = cm.createEntity("Enemy ");

	CHealth& healthP = cm.addComponent<CHealth>( player );
	CHealth& healthE = cm.addComponent<CHealth>( enemy );
	healthP.setHp(10);
	healthE.setHp(1000);

	CMotion& motionP = cm.addComponent<CMotion>( player );
	CMotion& motionE = cm.addComponent<CMotion>( enemy );
	MathTypes::Vec3 posP = { 5, 2, -5 };
	posP[0] = 5; posP[1] = 15; posP[2] = -5;
	motionP.setPos( posP );
	MathTypes::Vec3 posE = { -2, 42, -6 };
	motionP.setPos( posE );

	CKeyListener& kl = cm.addComponent<CKeyListener>( player );
	kl.setHandler( []( short keyCode,  const Entity& entity, ComponentManager& cm )
	{
		if ( keyCode == 'a')
		{
			auto& motion = cm.getComponent<Component::CMotion>( entity );
			if (motion)
			{
				MathTypes::Vec3 pos = motion->getPos();
				pos[0]--;
				motion->setPos( pos );
				printf("'a' was hit! new position: %f, %f, %f\n", pos[0], pos[1], pos[2] );
			}
		}
	}
	);

	// preint out the ComponentManager state
	int counter = 0;

	printf("Entities: \n");
	for ( auto& entityIt  = cm.begin<Entity>(); entityIt != cm.end<Entity>(); ++entityIt )
	{
		auto& entity = *entityIt;

		if ( !entity ) break;
		
		printf("%d: name: %s\n", 
			counter++, entity->getDebugString().c_str() );
	}

	printf("\nCHealth members: \n");
	counter = 0;
	for ( auto& healthIt  = cm.begin<CHealth>(); healthIt != cm.end<CHealth>(); ++healthIt )
	{
		auto& health = *healthIt;

		if ( !health ) break;
		
		printf("%d: owner: %s\t HP Data: %d\n", 
			counter++, health->getOwner().getDebugString().c_str(), health->getHp() );
	}

	printf("\nCMotion members: \n");
	counter = 0;
	for ( auto& motionIt  = cm.begin<CMotion>(); motionIt != cm.end<CMotion>(); ++motionIt )
	{
		auto& motion = *motionIt;

		if ( !motion ) break;
		
		const MathTypes::Vec3& pos = motion->getPos();
		printf("%d: owner: %s\t Pos Data: %f, %f, %f\n", 
			counter++, motion->getOwner().getDebugString().c_str(), pos[0], pos[1], pos[2] );
	}

	printf("\nCKeyListener members: \n");
	counter = 0;
	for ( auto& keyIt  = cm.begin<CKeyListener>(); keyIt != cm.end<CKeyListener>(); ++keyIt )
	{
		auto& key = *keyIt;

		if ( !key ) break;
		
		printf("%d: owner: %s\t Key handled: 'a'\n" , counter++, key->getOwner().getDebugString().c_str() );
		key->handleKey('a', cm);
	}
}