#pragma once
#include <vector>
#include "Functor.h"
#include "GameObjectData.h"

namespace Function
{

template <class TData>
class Processor
{
public:
	typedef Functor<TData> DataFunction;
	typedef std::vector< Data::GameObjectData* > DataVector;


	void operator+=( const TData& df )
	{
		m_dataVector.push_back(&df);
	}

	void process()
	{
		std:for_each( m_dataVector.begin(), m_dataVector.end(), 
		[](const TData* d)
		{
			if (d != nullptr && d->enabled) m_functor(d);
		});
	}

	FunctionVector	m_functionVector;
	DataFunction	m_functor;
};

}