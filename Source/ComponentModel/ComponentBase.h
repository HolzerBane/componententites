#pragma once
#include <string>
#include "ComponentModel\Entity.h"

namespace ComponentModel
{

class ComponentBase
{
public:
	
	ComponentBase( const Entity& go )
		: m_owner( go )
	{ }

	const Entity& getOwner() const { return m_owner; }

    operator size_t()
    {
        return m_owner.getId();
    }

protected:
	const Entity&	m_owner;
	std::string debugString;
};

}