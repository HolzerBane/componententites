#pragma once
#include "ComponentModel\Entity.h"
#include "ComponentModel\ComponentBase.h"
#include "ComponentModel\ContainerHolder.h"
#include "ComponentModel\Components\ComponentList.h"
#include "ComponentModel\Components\Components.h"

#include "Meta.h"

namespace ComponentModel
{

class Entity;

class ComponentManager : public Meta::WrappedDerivator< ContainerHolder, ComponentList >,
						 public ContainerHolder<Entity>
{

public:

	Entity& createEntity( const std::string& debugStr = "")
	{
		Entity* entity = new Entity(debugStr);
		//ownership is passed to ContainerHolder
        ContainerHolder<Entity>::add(entity->getId(), entity);	

		return *entity;
	}

	template< class T >
	const std::unique_ptr<T>& getComponent( const Entity& go )
	{
        static_assert( std::is_base_of< ContainerHolder<T>, ComponentManager >::value, "ComponentManager needs to derive from contained component's type!");
		return typename ContainerHolder<T>::get(go.getId());
	}

    template< class T >
	T& getComponentRef( const Entity& go )
	{
        static_assert( std::is_base_of< ContainerHolder<T>, ComponentManager >::value, "ComponentManager needs to derive from contained component's type!");
		return typename ContainerHolder<T>::getRef(go.getId());
	}

	template< class T >
	T& addComponent( const Entity& owner )
	{
        static_assert( std::is_base_of< ContainerHolder<T>, ComponentManager >::value, "ComponentManager needs to derive from contained component's type!");
		static_assert( std::is_base_of< ComponentModel::ComponentBase, T >::value, "A Component must derive from ComponentBase!" );
		typename ContainerHolder<T>::add( owner.getId(), new T(owner));
		 
		return typename ContainerHolder<T>::getRef( owner.getId() );
	}

    /* iterators */
    template< class T >
    typename ContainerHolder<T>::TValPtrContainer::iterator begin() { return typename ContainerHolder<T>::begin(); }

    template< class T >
    typename ContainerHolder<T>::TValPtrContainer::iterator end() { return typename ContainerHolder<T>::end(); }

	template< class T >
	const typename ContainerHolder<T>::TValPtrContainer& getContainer() const { return typename ContainerHolder<T>::getContainer(); }

	ComponentManager()
	{
	
	}

private:
	ComponentManager( const ComponentManager& ) {}
};

}


