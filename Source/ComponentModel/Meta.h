#pragma once

namespace Meta
{

// Empty type used for default template parameter
class NullType {};

// Generic Head|Tail list used as a TypeList 
template< typename H, class T >
struct TypeList {
    typedef H head_type;
    typedef T tail_type;
};


// MakeTypeList for easier declaration 
// workd for up to 10 types
// still better than using macro or spamming TypeList
// basically the Loki lib's implementation: http://loki-lib.sourceforge.net/
template<
	typename T1 = NullType,
	typename T2 = NullType,
	typename T3 = NullType,
	typename T4 = NullType,
	typename T5 = NullType,
	typename T6 = NullType,
	typename T7 = NullType,
	typename T8 = NullType
> 
struct MakeTypeList
{
	typedef typename MakeTypeList
	<	T2,
		T3,
		T4,
		T5,
		T6,
		T7,
		T8
	>::Types TTail;

	typedef TypeList<T1, TTail > Types;
};

template<>
struct MakeTypeList<>
{
    typedef NullType Types;
};

// Derivator type for deriving from an arbitrary number of types
template< class TL >
struct Derivator : public TL {};

template< typename H, typename T >
struct Derivator< TypeList<H,T> > : public H
                                  , public Derivator<T> 
{};

// Derivator with wrappend ancestors
template< template<class> class THolder, class TL >
struct WrappedDerivator : public THolder<TL> {};


template< template<class> class THolder, typename H, typename T >
struct WrappedDerivator< THolder, TypeList<H,T> > : public THolder<H>
                                  , public WrappedDerivator< THolder, T> 
{};


/*
///////////////////////////////////////////////////////////////////////////////
Example usage of TypeList, MakeTypeList and WrappedDerivator
///////////////////////////////////////////////////////////////////////////////

class AA{ public: int a; };
class BB{ public: int b; };
class CC{ public: int c; };
class DD{ public: int d; };

template< class T>
class Holder
{
public:

	typedef T HeldType;

	T& get() { return m_member; }

private:
	T m_member;
};

typedef MakeTypeList<AA, BB, CC, DD >::Types ABCD;

class Manager : public WrappedDerivator< Holder, ABCD >
{
public:

	template< class T >
	T& get( )
	{
		return typename Holder<T>::get();
	}

	template< class T >
	void set( )
};
///////////////////////////////////////////////////////////////////////////////

int main()
{
	Manager manager;
	
	AA& aa = manage.getM<AA>();
	BB& bb = manage.getM<BB>();
}

*/
}