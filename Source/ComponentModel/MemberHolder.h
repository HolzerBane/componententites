#pragma once
#include <memory>

template < class TValue >
class MemberHolder
{
public:
    typedef std::shared_ptr< TValue >		TValPtr;

protected:

    void set( const TValPtr vPtr) { m_member = vPtr; }

	const TValPtr _get( ) const
	{  
		return m_member;
	}

    TValue& getRef( ) const
	{  
		return *m_member;
	}

private:
	TValPtr		m_member;
};