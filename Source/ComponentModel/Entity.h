#pragma once
#include <vector>
#include <string>

namespace ComponentModel
{

class Entity
{
private:

public:

	Entity( const std::string& debugString = "")
        : m_debugString(debugString)
	{
		static unsigned idGen = 0;
		m_id = idGen++;
	}

	unsigned getId() const { return m_id; }
	bool isActive() const { return m_active; }
	void setActive( const bool active ) { m_active = active; }

	const std::string getDebugString() const { return m_debugString; }

private:
	unsigned			m_id;
	bool				m_active;
	std::string			m_debugString;
};

}