#pragma once
#include <Functional>
#include "ComponentModel/ComponentManager.h"
#include "Types.h"

namespace ComponentModel
{
	class ComponentManager;
}

namespace Component
{

class CKeyListener : public ComponentModel::ComponentBase
{
public:
	typedef std::function< void(short, const ComponentModel::Entity&, ComponentModel::ComponentManager& )> KeyListenerDelegate;

    CKeyListener( const ComponentModel::Entity& go ) : ComponentModel::ComponentBase(go)	{ }

    void setHandler( KeyListenerDelegate handler ) { m_handler = handler; }
    void handleKey( short key, ComponentModel::ComponentManager& cm ) { m_handler( key, m_owner, cm); }

private:
    KeyListenerDelegate m_handler;
};

}