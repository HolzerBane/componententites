#pragma once
#include "Meta.h"

namespace Component
{
	class CMotion;
	class CHealth;
	class CKeyListener;
}

namespace ComponentModel
{
	typedef Meta::MakeTypeList
	< 
		Component::CMotion, 
		Component::CHealth, 
		Component::CKeyListener 
	>::Types
		ComponentList;
}