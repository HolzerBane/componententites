#pragma once
#include <array>
#include "ComponentModel/ComponentBase.h"
#include "ComponentModel/Entity.h"
#include "ComponentModel/Components/Types.h"

namespace Component
{

class CMotion : public ComponentModel::ComponentBase
{
public:

	enum Axis 
	{
		X = 0,
		Y,
		Z
	};

	CMotion( const ComponentModel::Entity& go ) 
		: ComponentBase(go)
	{
		m_position.assign(0.f);
		m_rotation.assign(0.f);
		m_velocity.assign(0.f);
	}

	const MathTypes::Vec3& getPos( ) const { return m_position; }
	const MathTypes::Vec3& getRot( ) const { return m_rotation; }
	const MathTypes::Vec3& getVel( ) const { return m_velocity; }

    void setPos( const MathTypes::Vec3& val ) { m_position = val; }
	void setRot( const MathTypes::Vec3& val ) { m_rotation = val; }
	void setVel( const MathTypes::Vec3& val ) { m_velocity = val; }

private:
	MathTypes::Vec3 m_position;
	MathTypes::Vec3 m_rotation; 
	MathTypes::Vec3 m_velocity; 

};

}