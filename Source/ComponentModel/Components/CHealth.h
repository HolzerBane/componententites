#pragma once
#include "ComponentModel/ComponentBase.h"
#include "ComponentModel/Entity.h"

namespace Component
{

class CHealth : public ComponentModel::ComponentBase
{
public:

	CHealth( const ComponentModel::Entity& owner ) 
		: ComponentModel::ComponentBase(owner)
	{ }

	void kill() { m_alive = false; }
	bool isAlive() const { return m_alive; }
	void setHp( const int hp) { m_hp = hp; }
	int getHp( ) const  { return m_hp; }

private:
	int		m_hp;
	int		m_maxHp;
	bool	m_alive;
};
}