#pragma once
#include "ComponentModel/ComponentBase.h"
#include <array>

namespace Component
{

class CLocation : public ComponentBase
{
public:

	typedef std::array< float, 3 > Vec3;

	enum Axis 
	{
		X = 0,
		Y,
		Z
	};

	CLocation( GameObject& go ) 
		: ComponentBase(go)
	{
		m_position.assign(0.f);
		m_rotation.assign(0.f);
		m_velocity.assign(0.f);
	}

	const Vec3& getPos( ) const { return m_position; }
	const Vec3& getRot( ) const { return m_rotation; }
	const Vec3& getVel( ) const { return m_velocity; }

    void setPos( const Vec3& val ) { m_position = val; }
	void setRot( const Vec3& val ) { m_rotation = val; }
	void setVel( const Vec3& val ) { m_velocity = val; }

private:
	Vec3 m_position;
	Vec3 m_rotation; // euler, lol
	Vec3 m_velocity; 

};

}