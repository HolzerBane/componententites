#pragma once

namespace ComponentModel
{
	namespace Settings
	{
		const unsigned long StartingPoolSize = 20;
	}
}