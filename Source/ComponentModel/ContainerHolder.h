#pragma once
#include <vector>
#include <memory>
#include <assert.h>
#include "ComponentModel\Settings.h"

template < class TValue >
class ContainerHolder
{
public:
	typedef TValue						 HeldType;
    typedef std::unique_ptr< HeldType >	 TValPtr;
	typedef std::vector< TValPtr >		 TValPtrContainer;
	typedef std::vector< const TValPtr > TValConstPtrContainer;

	ContainerHolder()
	{
		m_container.resize( ComponentModel::Settings::StartingPoolSize );
	}

protected:
	const TValPtr& get( const unsigned id ) 
	{  
		assert( id < m_container.size() );
		return m_container[id];
	}

    TValue& getRef( const unsigned id ) 
	{  
		assert( id < m_container.size() );
		return *m_container[id];
	}


	void add( const unsigned id, HeldType* element )
	{
		assert( id < m_container.size() );
		m_container[id].reset( element );
	}

	void resize( const size_t size )
	{
		m_container.resize( size );
	}
    
	typename TValPtrContainer::iterator		begin()		{ return m_container.begin(); }
	typename TValPtrContainer::iterator		end()			{ return m_container.end(); }

	typename TValPtrContainer::const_iterator	begin() const	{ return m_container.cbegin(); }
	typename TValPtrContainer::const_iterator	end()	const	{ return m_container.cend(); }

	const TValPtrContainer& getContainer() const { return m_container; }

private:
	TValPtrContainer		m_container;
};